﻿using EF.Core.BLL.Services;
using EF.Core.Common.DTO.Project;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.Core.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;
        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<ActionResult<List<ProjectViewModelDTO>>> GetAll()
        {
            return Ok(await _projectService.GetTeams());
        }

        [HttpPost("create")]
        public async Task<ActionResult<ProjectViewModelDTO>> CreateTeam([FromBody] ProjectCreateDTO dto)
        {
            await _projectService.CreateProject(dto);
            return Ok();
        }
    }
}
