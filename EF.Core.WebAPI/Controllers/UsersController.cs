﻿using EF.Core.BLL.Services;
using EF.Core.Common.DTO.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.Core.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;
        public UsersController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<UserViewModelDTO>>> GetAll()
        {
            return Ok(await _userService.GetUsers());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserViewModelDTO>> GetById(int id)
        {
            return Ok(await _userService.GetById(id));
        }

        [HttpPost("create")]
        public async Task<ActionResult> CreateUser([FromBody] UserCreatedDTO userDto)
        {
            await _userService.CreateUser(userDto);
            return Ok();
        }

        [HttpPut("update/{id}")]
        public async Task<ActionResult> UpdateTeam([FromBody] UserCreatedDTO updateDTO, int id)
        {
            await _userService.UpdateUser(updateDTO, id);
            return Ok();
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult> DeleteTeam(int id)
        {
            await _userService.DeleteUser(id);
            return Ok();
        }

    }
}
