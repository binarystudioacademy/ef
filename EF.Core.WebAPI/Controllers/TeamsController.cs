﻿using EF.Core.BLL.Services;
using EF.Core.Common.DTO.Team;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.Core.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<List<TeamViewModelDTO>>> GetAll()
        {
            return Ok(await _teamService.GetTeams());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamViewModelDTO>> GetById(int id)
        {
            return Ok(await _teamService.GetById(id));
        }

        [HttpPost("create")]
        public async Task<ActionResult<TeamViewModelDTO>> CreateTeam([FromBody] TeamCreatedDTO teamDto)
        {
            return Ok(await _teamService.CreateTeam(teamDto));
        }

        [HttpPut("update/{id}")]
        public async Task<ActionResult> UpdateTeam([FromBody] TeamCreatedDTO updateDTO, int id)
        {            
            await _teamService.UpdateTeam(updateDTO,id);
            return Ok();            
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult> DeleteTeam(int id)
        {
            await _teamService.DeleteTeam(id);
            return Ok();
        }
    }
}
