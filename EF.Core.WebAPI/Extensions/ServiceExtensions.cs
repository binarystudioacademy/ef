﻿using AutoMapper;
using EF.Core.BLL.MappingProfiles;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace EF.Core.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<ProjectProfile>();
                
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
