﻿namespace EF.Core.Common.DTO.Team
{
    public sealed class TeamCreatedDTO
    {
        public string Name { get; set; }
    }
}
