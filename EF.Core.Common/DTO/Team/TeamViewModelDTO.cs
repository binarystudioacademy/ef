﻿using System;

namespace EF.Core.Common.DTO.Team
{
    public class TeamViewModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
