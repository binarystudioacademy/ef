﻿using System;

namespace EF.Core.Common.DTO.Project
{
    public class ProjectViewModelDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
         
    }
}
