﻿using System;

namespace EF.Core.Common.DTO.User
{
    public sealed class UserCreatedDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }        
        public DateTime BirthDay { get; set; }
        public int? TeamId { get; set; }                
    }
}
