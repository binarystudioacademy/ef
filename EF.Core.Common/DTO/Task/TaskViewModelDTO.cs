﻿using EF.Core.DAL.Entities;
using System;

namespace EF.Core.Common.DTO.Task
{ 
    public class TaskViewModelDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState TaskState { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }        
    }
}