﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EF.Core.DAL.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaskStates",
                columns: table => new
                {
                    TaskStateId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskStates", x => x.TaskStateId);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Projects_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    TaskStateId = table.Column<int>(type: "int", nullable: true),
                    ProjectId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_TaskStates_TaskStateId",
                        column: x => x.TaskStateId,
                        principalTable: "TaskStates",
                        principalColumn: "TaskStateId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 1, 24, 8, 42, 30, 591, DateTimeKind.Local).AddTicks(4880), "D'Amore and Sons" },
                    { 10, new DateTime(2022, 1, 24, 8, 42, 30, 595, DateTimeKind.Local).AddTicks(845), "Raynor - Pfannerstill" },
                    { 9, new DateTime(2022, 1, 24, 8, 42, 30, 595, DateTimeKind.Local).AddTicks(687), "Parker and Sons" },
                    { 8, new DateTime(2022, 1, 24, 8, 42, 30, 595, DateTimeKind.Local).AddTicks(491), "Welch, Wolf and Blanda" },
                    { 7, new DateTime(2022, 1, 24, 8, 42, 30, 595, DateTimeKind.Local).AddTicks(255), "Osinski Group" },
                    { 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "testTeam" },
                    { 5, new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9818), "Kozey Inc" },
                    { 4, new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9581), "Hintz and Sons" },
                    { 3, new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9399), "Flatley Group" },
                    { 2, new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9065), "Franecki, Macejkovic and Langworth" },
                    { 6, new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9980), "Leuschke - Block" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 9, new DateTime(1977, 2, 18, 4, 17, 13, 0, DateTimeKind.Local).AddTicks(143), "Horace_Farrell80@yahoo.com", "Nelle.Adams44", "Farrell", new DateTime(2022, 1, 24, 8, 42, 30, 638, DateTimeKind.Local).AddTicks(3519), null },
                    { 1, new DateTime(1997, 10, 10, 3, 21, 14, 29, DateTimeKind.Local).AddTicks(4789), "Nathaniel_Reichert4@yahoo.com", "Micah.Toy68", "Reichert", new DateTime(2022, 1, 24, 8, 42, 30, 635, DateTimeKind.Local).AddTicks(4846), null },
                    { 2, new DateTime(1958, 4, 24, 20, 36, 45, 357, DateTimeKind.Local).AddTicks(2557), "Amelia40@yahoo.com", "Gerson81", "Sanford", new DateTime(2022, 1, 24, 8, 42, 30, 636, DateTimeKind.Local).AddTicks(4725), null },
                    { 3, new DateTime(1965, 12, 18, 7, 28, 12, 7, DateTimeKind.Local).AddTicks(9222), "Randall.Hartmann@yahoo.com", "Arvel_Schaefer", "Hartmann", new DateTime(2022, 1, 24, 8, 42, 30, 636, DateTimeKind.Local).AddTicks(6483), null },
                    { 4, new DateTime(1978, 4, 29, 11, 28, 40, 110, DateTimeKind.Local).AddTicks(1545), "Roderick.Willms17@hotmail.com", "Alison.Abshire2", "Willms", new DateTime(2022, 1, 24, 8, 42, 30, 636, DateTimeKind.Local).AddTicks(7978), null },
                    { 5, new DateTime(1959, 7, 23, 12, 43, 25, 231, DateTimeKind.Local).AddTicks(4716), "Dustin.Walter50@hotmail.com", "Eden_OHara", "Walter", new DateTime(2022, 1, 24, 8, 42, 30, 637, DateTimeKind.Local).AddTicks(7816), null },
                    { 6, new DateTime(1967, 4, 2, 8, 42, 46, 96, DateTimeKind.Local).AddTicks(8015), "Stephen.Zboncak@yahoo.com", "Wilma.Bergstrom", "Zboncak", new DateTime(2022, 1, 24, 8, 42, 30, 637, DateTimeKind.Local).AddTicks(9370), null },
                    { 7, new DateTime(1963, 9, 23, 15, 32, 24, 399, DateTimeKind.Local).AddTicks(6554), "Beth.Grady87@yahoo.com", "Raegan.Hermann", "Grady", new DateTime(2022, 1, 24, 8, 42, 30, 638, DateTimeKind.Local).AddTicks(812), null },
                    { 8, new DateTime(1961, 9, 30, 11, 30, 16, 245, DateTimeKind.Local).AddTicks(2376), "Hugh_Price@yahoo.com", "Gina.Lindgren", "Price", new DateTime(2022, 1, 24, 8, 42, 30, 638, DateTimeKind.Local).AddTicks(2122), null },
                    { 10, new DateTime(1984, 7, 26, 14, 42, 41, 586, DateTimeKind.Local).AddTicks(9936), "Eddie6@hotmail.com", "Mohammad.Bogan", "Dooley", new DateTime(2022, 1, 24, 8, 42, 30, 638, DateTimeKind.Local).AddTicks(4942), null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_UserId",
                table: "Projects",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_TaskStateId",
                table: "Tasks",
                column: "TaskStateId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_UserId",
                table: "Tasks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "TaskStates");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
