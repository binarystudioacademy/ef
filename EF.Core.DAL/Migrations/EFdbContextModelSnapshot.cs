﻿// <auto-generated />
using System;
using EF.Core.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EF.Core.DAL.Migrations
{
    [DbContext(typeof(EFdbContext))]
    partial class EFdbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.13")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EF.Core.DAL.Entities.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Deadline")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TeamId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TeamId");

                    b.HasIndex("UserId");

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.Task", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("FinishedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ProjectId")
                        .HasColumnType("int");

                    b.Property<int?>("TaskStateId")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ProjectId");

                    b.HasIndex("TaskStateId");

                    b.HasIndex("UserId");

                    b.ToTable("Tasks");
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.TaskState", b =>
                {
                    b.Property<int>("TaskStateId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("TaskStateId");

                    b.ToTable("TaskStates");
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Teams");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 591, DateTimeKind.Local).AddTicks(4880),
                            Name = "D'Amore and Sons"
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9065),
                            Name = "Franecki, Macejkovic and Langworth"
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9399),
                            Name = "Flatley Group"
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9581),
                            Name = "Hintz and Sons"
                        },
                        new
                        {
                            Id = 5,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9818),
                            Name = "Kozey Inc"
                        },
                        new
                        {
                            Id = 6,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 594, DateTimeKind.Local).AddTicks(9980),
                            Name = "Leuschke - Block"
                        },
                        new
                        {
                            Id = 7,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 595, DateTimeKind.Local).AddTicks(255),
                            Name = "Osinski Group"
                        },
                        new
                        {
                            Id = 8,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 595, DateTimeKind.Local).AddTicks(491),
                            Name = "Welch, Wolf and Blanda"
                        },
                        new
                        {
                            Id = 9,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 595, DateTimeKind.Local).AddTicks(687),
                            Name = "Parker and Sons"
                        },
                        new
                        {
                            Id = 10,
                            CreatedAt = new DateTime(2022, 1, 24, 8, 42, 30, 595, DateTimeKind.Local).AddTicks(845),
                            Name = "Raynor - Pfannerstill"
                        },
                        new
                        {
                            Id = 11,
                            CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "testTeam"
                        });
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("BirthDay")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("RegisteredAt")
                        .HasColumnType("datetime2");

                    b.Property<int?>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TeamId");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            BirthDay = new DateTime(1997, 10, 10, 3, 21, 14, 29, DateTimeKind.Local).AddTicks(4789),
                            Email = "Nathaniel_Reichert4@yahoo.com",
                            FirstName = "Micah.Toy68",
                            LastName = "Reichert",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 635, DateTimeKind.Local).AddTicks(4846)
                        },
                        new
                        {
                            Id = 2,
                            BirthDay = new DateTime(1958, 4, 24, 20, 36, 45, 357, DateTimeKind.Local).AddTicks(2557),
                            Email = "Amelia40@yahoo.com",
                            FirstName = "Gerson81",
                            LastName = "Sanford",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 636, DateTimeKind.Local).AddTicks(4725)
                        },
                        new
                        {
                            Id = 3,
                            BirthDay = new DateTime(1965, 12, 18, 7, 28, 12, 7, DateTimeKind.Local).AddTicks(9222),
                            Email = "Randall.Hartmann@yahoo.com",
                            FirstName = "Arvel_Schaefer",
                            LastName = "Hartmann",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 636, DateTimeKind.Local).AddTicks(6483)
                        },
                        new
                        {
                            Id = 4,
                            BirthDay = new DateTime(1978, 4, 29, 11, 28, 40, 110, DateTimeKind.Local).AddTicks(1545),
                            Email = "Roderick.Willms17@hotmail.com",
                            FirstName = "Alison.Abshire2",
                            LastName = "Willms",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 636, DateTimeKind.Local).AddTicks(7978)
                        },
                        new
                        {
                            Id = 5,
                            BirthDay = new DateTime(1959, 7, 23, 12, 43, 25, 231, DateTimeKind.Local).AddTicks(4716),
                            Email = "Dustin.Walter50@hotmail.com",
                            FirstName = "Eden_OHara",
                            LastName = "Walter",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 637, DateTimeKind.Local).AddTicks(7816)
                        },
                        new
                        {
                            Id = 6,
                            BirthDay = new DateTime(1967, 4, 2, 8, 42, 46, 96, DateTimeKind.Local).AddTicks(8015),
                            Email = "Stephen.Zboncak@yahoo.com",
                            FirstName = "Wilma.Bergstrom",
                            LastName = "Zboncak",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 637, DateTimeKind.Local).AddTicks(9370)
                        },
                        new
                        {
                            Id = 7,
                            BirthDay = new DateTime(1963, 9, 23, 15, 32, 24, 399, DateTimeKind.Local).AddTicks(6554),
                            Email = "Beth.Grady87@yahoo.com",
                            FirstName = "Raegan.Hermann",
                            LastName = "Grady",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 638, DateTimeKind.Local).AddTicks(812)
                        },
                        new
                        {
                            Id = 8,
                            BirthDay = new DateTime(1961, 9, 30, 11, 30, 16, 245, DateTimeKind.Local).AddTicks(2376),
                            Email = "Hugh_Price@yahoo.com",
                            FirstName = "Gina.Lindgren",
                            LastName = "Price",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 638, DateTimeKind.Local).AddTicks(2122)
                        },
                        new
                        {
                            Id = 9,
                            BirthDay = new DateTime(1977, 2, 18, 4, 17, 13, 0, DateTimeKind.Local).AddTicks(143),
                            Email = "Horace_Farrell80@yahoo.com",
                            FirstName = "Nelle.Adams44",
                            LastName = "Farrell",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 638, DateTimeKind.Local).AddTicks(3519)
                        },
                        new
                        {
                            Id = 10,
                            BirthDay = new DateTime(1984, 7, 26, 14, 42, 41, 586, DateTimeKind.Local).AddTicks(9936),
                            Email = "Eddie6@hotmail.com",
                            FirstName = "Mohammad.Bogan",
                            LastName = "Dooley",
                            RegisteredAt = new DateTime(2022, 1, 24, 8, 42, 30, 638, DateTimeKind.Local).AddTicks(4942)
                        });
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.Project", b =>
                {
                    b.HasOne("EF.Core.DAL.Entities.Team", "Team")
                        .WithMany("Projects")
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EF.Core.DAL.Entities.User", "User")
                        .WithMany("Projects")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Team");

                    b.Navigation("User");
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.Task", b =>
                {
                    b.HasOne("EF.Core.DAL.Entities.Project", "Project")
                        .WithMany("Tasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EF.Core.DAL.Entities.TaskState", "TaskState")
                        .WithMany("Tasks")
                        .HasForeignKey("TaskStateId");

                    b.HasOne("EF.Core.DAL.Entities.User", "User")
                        .WithMany("Tasks")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.Navigation("Project");

                    b.Navigation("TaskState");

                    b.Navigation("User");
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.User", b =>
                {
                    b.HasOne("EF.Core.DAL.Entities.Team", "Team")
                        .WithMany("Users")
                        .HasForeignKey("TeamId");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.Project", b =>
                {
                    b.Navigation("Tasks");
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.TaskState", b =>
                {
                    b.Navigation("Tasks");
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.Team", b =>
                {
                    b.Navigation("Projects");

                    b.Navigation("Users");
                });

            modelBuilder.Entity("EF.Core.DAL.Entities.User", b =>
                {
                    b.Navigation("Projects");

                    b.Navigation("Tasks");
                });
#pragma warning restore 612, 618
        }
    }
}
