﻿using Bogus;
using EF.Core.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace EF.Core.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        private const int ENTITY_COUNT = 10;
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .HasOne(u => u.User)
                .WithMany(t => t.Tasks)
                .HasForeignKey(u => u.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasMany(t => t.Tasks)
                .WithOne(u => u.User)
                .HasForeignKey(t => t.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = GenerateRandomTeams();
            var users = GenerateRandomUsers();
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
        }

        private static ICollection<Team> GenerateRandomTeams()
        {
            int teamId = 1;

            var testTeamsFake = new Faker<Team>()
                .RuleFor(u => u.Id, f => teamId++)
                .RuleFor(u => u.Name, f => f.Company.CompanyName())
                .RuleFor(pi => pi.CreatedAt, f => DateTime.Now);

            var generatedTeamss = testTeamsFake.Generate(ENTITY_COUNT);
                       
            var myTeam = new Team
            {
                Id = teamId,
                Name = "testTeam",
            };

            generatedTeamss.Add(myTeam);

            return generatedTeamss;
        }
        private static ICollection<User> GenerateRandomUsers()
        {
            int userId = 1;

            var testUsersFake = new Faker<User>()
                .RuleFor(u => u.Id, f => userId++)
                .RuleFor(u => u.FirstName, f => f.Internet.UserName())
                .RuleFor(u => u.LastName, f => f.Person.LastName)
                .RuleFor(u => u.BirthDay, f => f.Person.DateOfBirth)
                .RuleFor(u => u.RegisteredAt, f => DateTime.Now)
                .RuleFor(u => u.Email, f => f.Person.Email);

            var generatedUsers = testUsersFake.Generate(ENTITY_COUNT);
                     
            return generatedUsers;

        }       
    }
}
