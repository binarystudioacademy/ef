﻿using EF.Core.DAL.Entities.Abstract;
using System;

namespace EF.Core.DAL.Entities
{
    public class Task 
    {     
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
              
        public int UserId { get; set; }
        public User User { get; set; }
               
        public TaskState TaskState { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
