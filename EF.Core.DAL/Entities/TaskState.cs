﻿using System.Collections.Generic;

namespace EF.Core.DAL.Entities
{
    public class TaskState
    {
        public TaskStateId TaskStateId { get; set; }
        public string Name { get; set; }

        public List<Task> Tasks { get; set; }
    }
}
