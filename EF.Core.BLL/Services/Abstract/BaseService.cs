﻿using AutoMapper;
using EF.Core.DAL.Context;

namespace EF.Core.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly EFdbContext _context;
        private protected readonly IMapper _mapper;

        public BaseService(EFdbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
