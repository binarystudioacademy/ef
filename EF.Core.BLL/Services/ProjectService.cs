﻿using AutoMapper;
using EF.Core.BLL.Services.Abstract;
using EF.Core.Common.DTO.Project;
using EF.Core.DAL.Context;
using EF.Core.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.Core.BLL.Services
{
    public sealed class ProjectService : BaseService
    {
        public ProjectService(EFdbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<List<ProjectViewModelDTO>> GetTeams()
        {
            var teams = await _context.Projects.ToListAsync();
            var result = _mapper.Map<List<ProjectViewModelDTO>>(teams);
            return result;
        }

        public async Task<ProjectViewModelDTO> GetById(int id)
        {
            var team = await _context.Projects.AsNoTracking()
                 .FirstOrDefaultAsync(t => t.Id == id);

            return _mapper.Map<ProjectViewModelDTO>(team);
        }

        public async System.Threading.Tasks.Task CreateProject(ProjectCreateDTO dto)
        {
            var project = _mapper.Map<Project>(dto);

            await _context.Projects.AddAsync(project);
            await _context.SaveChangesAsync();            
        }

        //public async System.Threading.Tasks.Task UpdateTeam(TeamCreatedDTO teamDto, int teamId)
        //{
        //    var postTeam = await _context.Teams.FirstOrDefaultAsync(t => t.Id == teamId);

        //    if (postTeam == null)
        //    {
        //        throw new NotFoundException(nameof(Team), teamId);
        //    }

        //    postTeam.Name = teamDto.Name;

        //    _context.Teams.Update(postTeam);
        //    await _context.SaveChangesAsync();
        //}

        //public async System.Threading.Tasks.Task DeleteTeam(int id)
        //{
        //    var team = await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);
        //    if (team == null)
        //    {
        //        throw new NotFoundException(nameof(Team), id);
        //    }

        //    _context.Teams.Remove(team);
        //    await _context.SaveChangesAsync();
        //}



    }
}
