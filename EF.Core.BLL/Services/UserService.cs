﻿using AutoMapper;
using EF.Core.BLL.Exceptions;
using EF.Core.BLL.Services.Abstract;
using EF.Core.Common.DTO.User;
using EF.Core.DAL.Context;
using EF.Core.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.Core.BLL.Services
{
    public sealed class UserService : BaseService
    {
        public UserService(EFdbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<List<UserViewModelDTO>> GetUsers()
        {
            var users = await _context.Users.ToListAsync();
            var result = _mapper.Map<List<UserViewModelDTO>>(users);
            return result;
        }

        public async Task<UserViewModelDTO> GetById(int id)
        {
            var user = await _context.Users.AsNoTracking()
                 .FirstOrDefaultAsync(t => t.Id == id);

            return _mapper.Map<UserViewModelDTO>(user);
        }

        public async System.Threading.Tasks.Task CreateUser(UserCreatedDTO userDto)
        {
            var newUser = new User
            {
                FirstName = userDto.FirstName,
                LastName = userDto.LastName,
                Email = userDto.Email,
                BirthDay = userDto.BirthDay,
                TeamId = userDto.TeamId
            };

            _context.Users.Add(newUser);

            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task UpdateUser(UserCreatedDTO userDto, int userId)
        {
            var user = await _context.Users.FirstOrDefaultAsync(t => t.Id == userId);

            if (user == null)
            {
                throw new NotFoundException(nameof(User), userId);
            }

            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteUser(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(t => t.Id == id);
            if (user == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }
    }
}
