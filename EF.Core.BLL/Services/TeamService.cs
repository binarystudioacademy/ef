﻿using AutoMapper;
using EF.Core.BLL.Exceptions;
using EF.Core.BLL.Services.Abstract;
using EF.Core.Common.DTO.Team;
using EF.Core.DAL.Context;
using EF.Core.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.Core.BLL.Services
{
    public sealed class TeamService : BaseService
    {
        public TeamService(EFdbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<List<TeamViewModelDTO>> GetTeams()
        {
            var teams = await _context.Teams.ToListAsync();
            var result = _mapper.Map<List<TeamViewModelDTO>>(teams);
            return result;
        }

        public async Task<TeamViewModelDTO> GetById(int id)
        {
            var team = await _context.Teams.AsNoTracking()
                 .FirstOrDefaultAsync(t => t.Id == id);

            return _mapper.Map<TeamViewModelDTO>(team);
        }

        public async Task<TeamViewModelDTO> CreateTeam(TeamCreatedDTO teamDto)
        {
            var newTeam = new Team
            {
                Name = teamDto.Name,
                CreatedAt = DateTime.Now
            };

            _context.Teams.Add(newTeam);

            await _context.SaveChangesAsync();

            return _mapper.Map<TeamViewModelDTO>(newTeam);
        }

        public async System.Threading.Tasks.Task UpdateTeam(TeamCreatedDTO teamDto, int teamId)
        {
            var postTeam = await _context.Teams.FirstOrDefaultAsync(t => t.Id == teamId);

            if (postTeam == null)
            {
                throw new NotFoundException(nameof(Team), teamId);
            }

            postTeam.Name = teamDto.Name;

            _context.Teams.Update(postTeam);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteTeam(int id)
        {
            var team = await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }
    }
}
