﻿using AutoMapper;
using EF.Core.Common.DTO.User;
using EF.Core.DAL.Entities;

namespace EF.Core.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserViewModelDTO>();
            CreateMap<UserCreatedDTO, User>();            
        }
    }
}
