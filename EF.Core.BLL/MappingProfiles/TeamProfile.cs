﻿using AutoMapper;
using EF.Core.Common.DTO.Team;
using EF.Core.DAL.Entities;

namespace EF.Core.BLL.MappingProfiles
{
    public sealed class TeamProfile: Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamViewModelDTO>();
            CreateMap<TeamCreatedDTO, TeamUpdateDTO>();
        }
    }
}
