﻿using AutoMapper;
using EF.Core.Common.DTO.Project;
using EF.Core.DAL.Entities;
using System;

namespace EF.Core.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectViewModelDTO>();
            CreateMap<ProjectCreateDTO, Project>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(s => DateTime.Now));            
        }
    }
}
